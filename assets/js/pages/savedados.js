$(document).ready(function()
{
	var url = $('#url').val();

	$('.ui.form').on('submit', function()
	{
		var checks = $('input[type="checkbox"]');
		checks.each(function()
		{
			if($(this).val() == 'on')
				$(this).val(1);
			else
				$(this).val(0);
		});

		if(!checkRequired())
			return false;

		$(this).addClass('loading');//
	});

	function checkRequired()
	{
		let selects = $('.select-input.required');
		let submit = true;

		selects.each(function () {

			if($('#input-'+$(this).data('campotabela')).val().length <= 0)
			{
				submit = false;
				$(this)
					.popup({
						popup : $('.custom.popup'),
						on    : 'hover'
					})
				;
				$(this).css('border-color', 'red');
			}
			else
			{
				$(this).css('border-color', 'none');
				$(this).popup({});
			}
		});

		return submit;
	}


	$('.select-medida').on('change', function()
	{
		var item = $(this).find('option:checked');
		$(this).prop('title', item.data('desc'));
		$(this).data('idmedida', item.val());

		if(item.hasClass('medida-padrao'))
		{
			return;
		}



		var campotabela = $(this).data('campotabela');
		var padrao = $('#'+campotabela);

		var conversao = 0;

		if(item.data('funcao'))
		{
			conversao = converter(item.data('funcao'), padrao.val(), item.text());
			padrao.val(conversao);
			return;
		}

		if(item.data('op') == '*')
			conversao = padrao.val() * item.data('prop');
		else if(item.data('op') == '/')
			conversao = padrao.val() / item.data('prop');

		padrao.val(conversao);
	});

	$('.type-select').on('change', function()
	{
		var campo = $(this).prop('id');
		var id = $('#input-'+campo).val();

		if(id.length == 0)
			$('#field-select-'+campo).find('.botaonovo-space[data-edicao="1"]').addClass('disabled');
		else
			$('#field-select-'+campo).find('.botaonovo-space[data-edicao="1"]').removeClass('disabled');

		var item = $(this).find('.item[data-value="'+id+'"]');

		$('#info-'+campo).prop('title', item.data('title'));
	});

	function restringirValores(campo, valor)
	{
		let campos = $('.select-input[data-camporestricao="'+campo+'"]');

		if(campos.length ==0)
			return;

		campos.each(function ()
		{
			campo = $(this);
			let campotabela = campo.data('campotabela');
			let coltab = campo.data('coluna-restricao');

			$.post(url+'restringir',
				{campotabela: campotabela, coltab: coltab, valorrestricao: valor, modulo: $('#modulo').val(), escala: campo.data('escala')},
				function (dados) {
				loadingSelect($('#'+campotabela));
				carregarSelect(campotabela, dados, function () {
					let valpadrao = $('#'+campotabela).data('padrao');
					if($('#'+campotabela).find('.item[data-value="'+valpadrao+'"]').length > 0)
						setTimeout(function (){$('#'+campotabela).dropdown('set selected', valpadrao);}, 100)
				});
			});
		});
	}

	$('.itself-check').on('change', function()
	{
		var campotabela = $(this).data('campotabela');

		if($(this).is(':checked'))
			$('#'+campotabela).addClass('disabled');
		else {
			$('#' + campotabela).removeClass('disabled');
			return;
		}

		$('#input-'+campotabela).val('@atual');
	});

	$(document).on('click', '.novotipos', function(e) {e.preventDefault();novotipo($(this), 'tipo', $(this).data('campotabela'));});

	$(document).on('click', '.novosubtipos', function(e) {e.preventDefault(); novotipo($(this), 'subtipo', $(this).data('campotabela'))});

	function novotipo(el, tabela)
	{
		var campotabela = el.data('campotabela');
		$('#btn-novo'+tabela).html('Adicionar');
		$('.form-novo'+tabela).find('.limpa').val('');

		if(el.data('edicao'))
		{
			let nome = $('#'+campotabela).find('.text').text();
			$('#'+tabela+'-nome').val(nome);
			$('#'+tabela+'-id').val($('#input-'+campotabela).val());
			$('#'+tabela+'-descricao').val($('#info-'+campotabela).prop('title'));
			$('#btn-novo'+tabela).data('edicao', 1);
			$('#btn-novo'+tabela).html('Editar');
		}

		if(tabela == 'tipo')
			$('#tipo-escala').val(el.data('escala'));
		else
		{
			var id = $('#input-'+el.data('escala')).val();
			if(id.length <= 0)
			{
				swal('Nenhum tipo selecionado');
				return;
			}

			$('#subtipo-idexterno').val(id);
		}

		$('#btn-novo'+tabela).data('campotabela', el.data('campotabela'));
		$('.modal-novo'+tabela).modal('show');
	}

	function savetipo(el, tabela)
	{
		var campotabela = el.data('campotabela');
		var acao = 'novotipo';

		if(el.data('edicao'))
			acao = 'editatipo';

		$('.form-novo'+tabela).addClass('loading');
		$.post(url+acao,
			{
				nome: $('#'+tabela+'-nome').val(),
				descricao: $('#'+tabela+'-descricao').val(),
				id: $('#'+tabela+'-id').val(),
				idtipo: $('#subtipo-idexterno').val(),
				escala: $('#'+tabela+'-escala').val(),
				tabelas: $('#'+tabela+'-tabela').val(),
				tabela: tabela+'s'
			},
			function (dados) {
				dados = JSON.parse(dados);

				if(el.data('edicao')) {
					$('#' + campotabela).find('.item[data-value="' + $('#' + tabela + '-id').val() + '"]').remove();
					dados.id = $('#' + tabela + '-id').val();
				}
				var option = '<div class="item item-space" data-value="'+dados.id+'" dat-a-title="'+$('#'+tabela+'-descricao').val()+'">'+$('#'+tabela+'-nome').val()+'</div>';
				$('#'+campotabela).find('.menu').append(option);
				$('#subtipo-idexterno').val('');


				setTimeout(function () {
					if(dados.sucesso)
					{
						$('#'+campotabela).dropdown('set selected', dados.id);
						$('#info-'+campotabela).prop('title', $('#'+tabela+'-descricao').val());
					}

					$('.form-novo'+tabela).removeClass('loading');
					swal(dados.mensagem);
				}, 300);
			});
	}

	$(document).on('click', '#btn-novotipo', function (e) {e.preventDefault(); savetipo($(this), 'tipo');});

	$(document).on('click', '#btn-novosubtipo', function (e) {e.preventDefault(); savetipo($(this), 'subtipo')});

	$('.input-space').on('focusout', function (e)
	{
		let formula = $(this).data('formula');
		let valorThis = $(this).val();

		disparaFormula(formula, valorThis);
	});

	$('.select-space').on('change', function(e)
	{
		let campotabela = $(this).prop('name');
		vinculaValores($('#'+campotabela).find('.item[data-value="'+$(this).val()+'"]'));
		restringirValores(campotabela, $('#input-'+campotabela).val());
	});

	function vinculaValores(el)
	{
		let alvo = el.data('campoalvo');

		if(!alvo)
			return;

		let valor = el.data('valorvin');

		let campoalvo = $('#'+alvo);
		if($('#idedicao').val() == '')
			setaValor(campoalvo, valor, el.data('modovin'));
	}

	function disparaFormula(formula, valorThis)
	{
		if(!formula)
			return;
		var funcoes = formula.split('|');
		for(let i = 0; i <= funcoes.length; i++)
		{
			if (funcoes[i] == '' || !funcoes[i])
				continue;

			var atual = funcoes[i].split('=>>');
			let alvo = atual[0].trim();
			let valor = atual[1];


			if (valor.indexOf('{%if%}') > -1)
			{
				var cond = valor.split('{%if%}')[1];
				var op = cond.split(';')[0];
				var valorIf = cond.split(';')[1];

				if (!verifyIf(valorThis, op, valorIf))
					continue;
				valor = valor.split('{%if%}')[2];
			}

			if(valor.indexOf('[') == -1)
			{
				console.log('alvo');
				console.log(alvo);
				$('#' + alvo).val(valor);
				continue;
			}


			let inicio = valor.indexOf('[');

			let fim = valor.indexOf(']');

			let campo = valor.substring(inicio + 1, fim);
			let valorClear = valor.replaceAll('[' + campo + ']', $('#' + campo).val());
			campo = valorClear;
			$('#' + alvo).val(campo);
		}
	}

	function verifyIf(val, op, cond)
	{
		let retorno;

		switch (op)
		{
			case '=':
				retorno = val == cond;
				break;
			case '!=':
				retorno = val !== cond;
				break;
			case '>':
				retorno = val > cond;
				break;
			case '<':
				retorno = val < cond;
				break;
		}

		return retorno;
	}

	var inputs = $('.select-input');
	inputs.each(function () {$(this).dropdown('set selected', $(this).data('padrao'));});

	$('.select-medida').trigger('change');
});

