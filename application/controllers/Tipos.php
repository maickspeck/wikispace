<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipos extends CI_Controller
{
	public function novotipo()
	{
		$tipo = array_filter($_POST);
		$tabela = $tipo['tabela'];
		unset($tipo['tabela']);
		$retorno['mensagem'] = 'Tipo inserido com sucesso';

		$inserted = $this->Database->insert($tipo, $tabela);
		$retorno['sucesso'] = true;
		if(!$inserted)
		{
			$retorno['sucesso'] = false;
			$retorno['mensagem'] = 'Erro ao criar novo tipo: ' . $this->db->error()['message'];
		}

		$retorno['id'] = $inserted;
		echo json_encode($retorno);
	}

	public function editatipo()
	{
		$tipo = array_filter($_POST);
		$tabela = $tipo['tabela'];
		unset($tipo['tabela']);
		unset($tipo['id']);
		unset($tipo['idtipo']);

		$retorno['sucesso'] = true;
		$retorno['mensagem'] = 'Tipo alterado com sucesso';

		$update = $this->Database->update($tipo, $tabela, $_POST['id']);
		if(!$update)
		{
			$retorno['sucesso'] = false;
			$retorno['mensagem'] = 'Erro ao editar tipo: ' . $this->db->error()['message'];
		}

		echo json_encode($retorno);
	}
}
