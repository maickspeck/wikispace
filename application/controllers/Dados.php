<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dados extends CI_Controller
{
	public function pagesave($mod, $idedicao='')
	{
		$dados = [];

		$modulo = $this->Modulos->getModulo($mod);

		if($idedicao)
			$dados = $this->Database->getBy($modulo->tabela, $idedicao);

		if(!empty($_GET))
		{
			$dados = $this->searchRegistro($modulo->tabela, $_GET);
			$idedicao = $dados['id'];
		}

		if(empty($dados) && (!empty($_GET) || $idedicao))
		{
			$_SESSION['mensagem']['status'] = 'error';
			$_SESSION['mensagem']['texto'] = 'Não foi encontrado nenhum registro com esses parametros.';
			header('Location: '.$this->config->item('hostFixo').'save/'.$modulo->nomeconfig);
			die;
		}

		$template = $this->Campos->montaFormulario($modulo, $dados);

		unset($_SESSION['preenchidos']);

		$retorno['modulo'] = $modulo;
		$retorno['campos'] = $template;
		$retorno['idedicao'] = $idedicao;

		$this->load->template('savedados', $retorno);
	}

	public function searchRegistro($tabela, $get){return $this->Database->getBy($tabela, current($get), key($get));}

	public function save()
	{
		$id = $_POST['idedicao'];
		$modulo = $_POST['modulo'];
		$tabela = $_POST['tabela'];
		$request = $_POST;

		unset($request['tabela']);
		unset($request['idedicao']);
		unset($request['modulo']);

		$block = $this->Gatilhos->beforeSave($request, $tabela, $modulo);

		$_SESSION['preenchidos'] = $request;

		if($block)
			header('Location: ' . $_SERVER['HTTP_REFERER']);

//		pre('passou');
		foreach ($request as $key => $value) {$request[$key] = trim($value);}

		if($id)
			$this->update($request, $tabela, $modulo, $id);
		else
			$this->insert($request, $tabela, $modulo);
	}

	public function insert($request, $tabela, $modulo)
	{
		$inserted = $this->Database->insert($request, $tabela);

		if(!$inserted) {
			setError($inserted, $this->db->error()['message']);
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		else
		{
			unset($_SESSION['preenchidos']);
			$_SESSION['mensagem']['status'] = 'success';
			$_SESSION['mensagem']['texto'] = 'Registro incluído com sucesso!';

			$this->Gatilhos->afterInsert($tabela, $request, $inserted);
			header('Location: '.$this->config->item('hostFixo').'save/'.$modulo.'/'.$inserted);
		}
	}

	public function update($request, $tabela, $modulo, $id)
	{
		$update = $this->Database->update($request, $tabela, $id);

		if(!$update)
			setError($update, $this->db->error()['message']);
		else
		{
			$_SESSION['mensagem']['status'] = 'success';
			$_SESSION['mensagem']['texto'] = 'Registro alterado com sucesso!';
		}

		header('Location: '.$this->config->item('hostFixo').'save/'.$modulo.'/'.$id);
	}

	public function getValoresRestringidos()
	{
		$coltab = explode('.', $_POST['coltab']);
		$tabela = $coltab[0];
		$coluna = $coltab[1];
		$valor = $_POST['valorrestricao'];
		$modulo = $this->Modulos->getModulo($_POST['modulo']);
		$escala = $this->input->post('escala');

		$valores = $this->Restricoes->getValoresRestricao($tabela, $coluna, $valor, $escala);
		if(empty($valores))
			die(false);

		$campo = $this->Campos->montaCampoEspecifico($modulo->tabela, $_POST['campotabela'], $modulo, $valores);


		echo $this->Campos->options;
	}
}
