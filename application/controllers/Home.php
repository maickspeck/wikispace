<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function index()
	{
		$this->load->template('login');
	}

	public function inserir()
	{
		$obj = (array) $this->input->post('obj');
		
		$tabela = $this->input->post('tabela');
		$this->UsuariosModel->inserir($obj, $tabela);
	}

	public function goHome()
	{
		$this->load->template('addPalpite');
	}
}
