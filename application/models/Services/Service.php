<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Model
{
	public $response;
	public $dom;

	public function getDados($url)
	{
//		error_reporting(0);
		$this->response = $this->ApiConnection->connect($url);
		$this->dom = new DOMDocument();
		@$this->dom->loadHTML($this->response['response']);
	}

	public function getInnerHtml( $node )
	{
		$innerHTML= '';
		$children = $node->childNodes;
		foreach ($children as $child) {
			$innerHTML .= $child->ownerDocument->saveXML( $child );
		}
		return $innerHTML;
	}
}
