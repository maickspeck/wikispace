<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JPL extends Service
{
	public function getDados($id)
	{
		parent::getDados('https://ssd.jpl.nasa.gov/sbdb.cgi?sstr='.$id);
//		/tr/td/a/font[text()='SMASSII spectral type']
		$finder = new DomXPath($this->dom);
		$trs = $finder->query("//table[@bgcolor='#0099FF'][2]/tr/td/table/tr");

		$found = false;
		$value = null;

		foreach ($trs as $tr)
		{
			$i = 0;
			foreach ($tr->childNodes as $td)
			{

				if(empty($td->childNodes))
					continue;

				$i++;
				foreach ($td->childNodes as $a)
				{
					foreach ($a->childNodes as $font)
					{
						if($font->nodeValue == 'SMASSII spectral type')
							$found = true;
					}

					if($found && $i == 3)
					{
						$value = $a->nodeValue;
						break;
					}
				}
			}
			if($found)
				break;
		}

		return $value;
	}
}
