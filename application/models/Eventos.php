<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Model
{
	public function cadastrar($data, $dataini, $datafim, $tabela, $idregistro, $tipoevento, $relevancia)
	{
		$evento['dataevento'] = $data;

		if($dataini)
			$evento['dataeventoinicio'] = $dataini;
		if($datafim)
			$evento['dataeventofim'] = $datafim;
		$evento['tabela'] = $tabela;
		$evento['idregistro'] = $idregistro;
		$evento['tipoevento'] = $tipoevento;
		$evento['graurelevancia'] = $relevancia;

		return $this->Database->insert($evento, 'eventos');
	}
}
