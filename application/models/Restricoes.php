<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restricoes extends CI_Model
{
	public function getValoresRestricao($tabela, $coluna, $valor, $escala='')
	{
		$this->db->select('id, nome');
		$this->db->where($coluna, $valor);
		if($escala)
			$this->db->where('escala', $escala);
		return $this->db->get($tabela)->result_array();
	}

	public function getRestricoes($tabela, $idmodulo)
	{
		$this->db->select('*');
		$this->db->where('tabela', $tabela);
		$this->db->where('idmodulo IS NULL');
		$this->db->or_where('idmodulo', $idmodulo);
		return $this->db->get('restricoes')->result_array();
	}
}

