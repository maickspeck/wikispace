<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gatilhos extends CI_Model
{
	public function beforeSave($dados, $tabela, $modulo)
	{
		$modulo = $this->Modulos->getModulo($modulo);

		if($this->Bloqueios->getBloqueiosModulo($modulo->id, $dados))
			return true;

		return $this->Bloqueios->verificaBloqueio($dados, $tabela);
	}

	public function afterInsert($tabela, $registro, $id, $ordem=0)
	{
		$msg = ''; $erro = 'N'; $acao = 'Eventos->cadastrar';

		foreach ($registro as $campo => $valor)
		{
			if($valor == '@atual')
			{
				$this->insertMySelf($tabela, $id, $campo);
				unset($registro[$campo]);
			}
		}

		switch ($tabela)
		{
			case 'cometas':
				if(!isset($registro['proximoperielio']) || $ordem !== 0)
					return;

				$data = DateTime::createFromFormat('Y-m-d', $registro['proximoperielio']);

				if(!$data || $data->format('Y-m-d') != $registro['proximoperielio'])
				{
					$this->Log->inserirLog($tabela, $id, 'afterInsert', 'Eventos->cadastrar', 'S', 'Data em formato inválido!');
					return;
				}

				$registro['proximoperielio'] .= ' 20:00:00';

				$inserted = $this->Eventos->cadastrar($registro['proximoperielio'], '', '', $tabela, $id, 'Periélio', 3);

				$msg = setError($inserted, $this->db->error()['message'], $erro);

				break;
			case 'constelacoes':
				switch ($ordem)
				{
					case 0:
						if(!isset($registro['meridiano']))
							return;

						$meridiano = $registro['meridiano'];

						if(strlen($meridiano) == 2)
						$meridiano = '2020-'.$meridiano.'-21';
						else if(strlen($meridiano) == 5)
						$meridiano = '2020-'.$meridiano;

						$data = DateTime::createFromFormat('Y-m-d', $meridiano);

						if(!$data || $data->format('Y-m-d') != $meridiano)
						{
						$this->Log->inserirLog($tabela, $id, 'afterInsert', 'Eventos->cadastrar', 'S', 'Data em formato inválido!', $meridiano);
						return;
						}

						$meridiano .= ' 20:00:00';

						$inserted = $this->Eventos->cadastrar($meridiano, '', '', $tabela, $id, 'Meridiano', 1);

						$msg = setError($inserted, $this->db->error()['message'], $erro);

						break;
					case 1:
						if(!isset($registro['genitivo']))
							return;

						$genitivo = $registro['genitivo'];
						$acao = 'Vinculos->insertVinculo';
						$this->Vinculos->insertVinculo('idconstelacao', 'sistemasestelares', $id, 'nome', $genitivo, 'add');

						$msg = setError($this->db->error()['message'] == '', $this->db->error()['message'], $erro);

						break;
				}
				break;
			default:
				return;
				break;
		}

		$this->Log->inserirLog($tabela, $id, 'afterInsert', $acao, $erro, $msg);

		if($ordem == 0)
			$this->afterInsert($tabela, $registro, $id, $ordem=1);
	}

	public function insertMySelf($tabela, $id, $campo)
	{
		$erro = 'N';
		$update = $this->Database->update($tabela, array($campo => $id), $id);

		$msg = setError($update, $this->db->error()['message'], $erro);

		$this->Log->inserirLog($tabela, $id, 'insertMySelf', 'db->update',$erro, $msg);
	}
}
