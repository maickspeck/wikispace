<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RoboBase extends CI_Model
{
	public $identif;
	public $tabela;
	public $idlogservico;
	public $erro;
	public $idregistro = null;

	public function iniciar($acao, $tabela, $identif, $idlogservico, $id=null)
	{
		$this->identif = $identif;
		$this->idregistro = $id;
		$this->tabela = $tabela;
		$this->idlogservico = $idlogservico;
		$this->erro['erro'] = '0';
		$this->erro['mensagem'] = null;
		$this->$acao();

	}

	public function gravarLog($acao, $idaux='', $msg='')
	{
		if($msg)
			$this->erro['mensagem'] = $msg;
		if(!$this->Log->gravarLogRobo($acao, $this->tabela, $this->idlogservico, $this->identif, $this->erro['erro'], $this->erro['mensagem'], $idaux))
			pre($this->db->error()['message']);

		$this->erro['erro'] = '0';
		$this->erro['mensagem'] = null;
	}

	public function setError($cond, &$msg='')
	{
		if($cond)
			return;
		$this->erro['erro'] = '1';
		$this->erro['mensagem'] = $msg;
	}

	public function gravarLogRequisicao($response, $value, $acao, $idaux='')
	{
		$msg['status'] = $response['status'];

		if($msg['status'] !== 200)
		{
			$msg['retorno'] = $response['response'];
			$this->setError(false);
		}
		else
			$msg['retorno'] = $value;

		$this->erro['mensagem'] = json_encode($msg);
		$this->gravarLog($acao, $idaux);
	}
}
