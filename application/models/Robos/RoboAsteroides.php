<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RoboAsteroides extends RoboBase
{
	public function __construct()
	{
		$this->load->model('Services/JPL');
	}

	public function alterar()
	{
		$asteroid = $this->Database->getBy('asteroides', $this->identif, array('numero' => $this->identif, 'idtipo2 IS NULL' => NULL));

		if(empty($asteroid)) {
			$this->setError(false, 'Nenhum asteroide encontrado');
			$this->gravarLog('Busca de Asteroide');
			return;
		}

		$tipo = $this->JPL->getDados($this->identif);

		if($tipo == '')
			return;

		$response = $this->JPL->response;
		$this->gravarLogRequisicao($response, $tipo,'Requisicao JPL');

		$idtipo = $this->Database->getBy('tipos', $tipo, array('nome' => $tipo, 'escala' => 'SMASSII'), 'id');

		if(empty($idtipo))
		{
			$arrTipo['nome'] = $tipo;
			$arrTipo['escala'] = 'SMASSII';
			$arrTipo['tabelas'] = 'asteroides';
			$idtipo = $this->Database->insert($arrTipo, 'tipos');

			$this->setError($idtipo, $this->db->error()['message']);
			$this->gravarLog('Inserir Tipo', $idtipo);
		}
		else
			$idtipo = $idtipo['id'];

		$update = $this->Database->Update(array('idtipo2' => $idtipo), 'asteroides', $this->identif, 'numero');

		$this->setError($update, $this->db->error()['message']);
		$this->gravarLog('Setar Tipo', $idtipo);
	}
}
