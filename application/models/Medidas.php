<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medidas extends CI_Model
{
	public function getConversoes($medida=false)
	{
		$this->db->select('conversoes.*, m1.sigla as siglade, m1.key as keyde, m2.sigla as siglapara, m2.key as keypara, m1.descricao as descricaode, m2.descricao as descricaopara');
		$this->db->join('medidas m1', 'conversoes.idmedidade = m1.id', 'left');
		$this->db->join('medidas m2', 'conversoes.idmedidapara = m2.id', 'left');
		if($medida)
		{
			$this->db->where('idmedidade', $medida);
			$this->db->or_where('idmedidapara', $medida);
		}
		return $this->db->get('conversoes')->result_array();
	}

	public function getConversaoCampo($idmedida, $conversoes, $campotabela)
	{
		$options = '';
		$arrMedidas = [];
		foreach ($conversoes as $key => $value)
		{
			if($value['idmedidade'] == $idmedida)
			{
				if(!in_array($value['idmedidade'], $arrMedidas))
				{
					array_push($arrMedidas, $idmedida);
					$options .= "<option class='medida-padrao' value=\"{$idmedida}\" data-funcao=\"{$value['funcao']}\" data-desc='{$value['descricaode']}' data-op='=' data-prop='1'>{$value['siglade']}</option>";
				}
				array_push($arrMedidas, $value['idmedidapara']);
				$options .= "<option value=\"{$value['idmedidapara']}\" data-funcao=\"{$value['funcao']}\" data-desc='{$value['descricaopara']}' data-op='/' data-prop='{$value['proporcao']}'>{$value['siglapara']}</option>";
			}
			else if($value['idmedidapara'] == $idmedida)
			{
				if(!in_array($value['idmedidapara'], $arrMedidas))
				{
					array_push($arrMedidas, $idmedida);
					$options .= "<option class='medida-padrao' value=\"{$idmedida}\" data-funcao=\"{$value['funcao']}\" data-desc=\"{$value['descricaopara']}\" data-op='=' data-prop='1'>{$value['siglapara']}</option>";
				}
				array_push($arrMedidas, $value['idmedidade']);
				$options .= "<option value=\"{$value['idmedidade']}\" data-desc='{$value['descricaode']}' data-funcao=\"{$value['funcao']}\" data-op='*' data-prop='{$value['proporcao']}'>{$value['siglade']}</option>";
			}


		}

		$select = "<div style='padding-bottom: 5px; float: right; width: 150px;'>
			<select style='margin-top:-5px;background: #5d5b57; color: white;' class='select-medida' data-campotabela='{$campotabela}'>
				{$options}
			</select>
		</div>";
		
		if(!empty($arrMedidas))
			return $select;
		else
			return "";
	}
}
