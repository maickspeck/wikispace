<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vinculos extends CI_Model
{
	public function getTiposComVinculos($tabela)
	{
		$this->db->select('campotabela');
		$this->db->where('tabela', $tabela);
		$this->db->group_by('campotabela');
		return $this->db->get('vinculostabaux')->result_array();
	}

	public function getVinculosTabAux($tabela, $campotabela)
	{
		$this->db->select('*');
		$this->db->where('campotabela', $campotabela);
		$this->db->where('tabela', $tabela);
		return $this->db->get('vinculostabaux')->result_array();
	}

	public function insertVinculo($campotabela, $tabela, $idvalor, $campoalvo, $valor, $modo='sub')
	{
		$vinculo['campotabela'] = $campotabela;
		$vinculo['tabela'] = $tabela;
		$vinculo['idvalor'] = $idvalor;
		$vinculo['campotabelaalvo'] = $campoalvo;
		$vinculo['valor'] = $valor;
		$vinculo['modo'] = $modo;

		return $this->Database->insert($vinculo, 'vinculostabaux');
	}
}

