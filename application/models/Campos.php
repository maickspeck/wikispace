<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campos extends CI_Model
{
	public $tabela;
	public $campotabela;
	public $idmedida;
	public $label;
	public $campoaux;
	public $escala;
	public $visualizacao;
	public $options = '';
	public $optionRecords = [];

	public $modulo;

	public function getCampos($tabela)
	{
		$this->db->select('camposconfig.*, medidas.tipocampo, medidas.sigla');
		$this->db->join('medidas', 'camposconfig.idmedida = medidas.id', 'left');
		$this->db->where('tabela', $tabela);
		$this->db->where('idmodulo IS NULL');
		$this->db->or_where('idmodulo', $this->modulo->id);
		$this->db->order_by('ordem');
		return $this->db->get('camposconfig')->result_array();
	}

	public function getCampo($campotabela, $tabela)
	{
		$this->db->select('camposconfig.*, medidas.tipocampo, medidas.sigla');
		$this->db->join('medidas', 'camposconfig.idmedida = medidas.id', 'left');
		$this->db->where('tabela', $tabela);
		$this->db->where('campotabela', $campotabela);
		$this->db->where('idmodulo IS NULL');
		$this->db->or_where('idmodulo', $this->modulo->id);
		return $this->db->get('camposconfig')->row_array();
	}

	public function montaFormulario($modulo, $dados=[])
	{
		$this->modulo = $modulo;

		$campos = $this->Campos->getCampos($modulo->tabela);

		$vinculos = $this->Vinculos->getTiposComVinculos($modulo->tabela);

		$restricoes = $this->Restricoes->getRestricoes($modulo->tabela, $this->modulo->id);

		$conversoes = $this->Medidas->getConversoes();

		$template = '<div class="ui custom popup top transition hidden">Campo Obrigatório</div>';

		foreach ($campos as $key => $campo)
		{
			$conversao = $this->Medidas->getConversaoCampo($campo['idmedida'], $conversoes, $campo['campotabela']);
			$template .= $this->Campos->montarInput($campo, $conversao, $vinculos, $restricoes, $dados);
		}
		return $template;
	}

	public function montaCampoEspecifico($tabela, $campotabela, $modulo, $optionRecords=[])
	{
		$this->modulo = $modulo;

		$campo = $this->Campos->getCampo($campotabela, $tabela);

		$vinculos = $this->Vinculos->getVinculosTabAux($tabela, $campotabela);

		$restricao = [$this->Database->getBy('restricoes', $campotabela, 'campotabela')];

		$conversoes = $this->Medidas->getConversoes($campo['idmedida']);

		$conversao = $this->Medidas->getConversaoCampo($campo['idmedida'], $conversoes, $campo['campotabela']);

		$this->optionRecords = $optionRecords;

		return $this->Campos->montarInput($campo, $conversao, $vinculos, $restricao, []);
	}

	public function montarInput($campo, $conversao, $vinculos=[], $restricoes=[], $dados=[])
	{
		$required = ""; $blocked = '';
		$label = $campo['label'] .':';

		if($campo['obrigatorio'])
		{
			$required = 'required';
			$label .= '*';
		}

		if(isset($dados[$campo['campotabela']]))
			$campo['padrao'] = $dados[$campo['campotabela']];
		else if(!empty($dados))
			$campo['padrao'] = null;

		$campoMod = explode(' = ', $this->modulo->where);

		if(isset($campoMod[1]) && $campo['campotabela'] == $campoMod[0])
		{
			$blocked = 'disabled';
			$campo['padrao'] = $campoMod[1];
		}

		if(isset($_SESSION['preenchidos'][$campo['campotabela']]))
			$campo['padrao'] = $_SESSION['preenchidos'][$campo['campotabela']];


		switch ($campo['tipocampo'])
		{
			case 'texto':
				return "<div class=\"field {$blocked}\">
    <label>{$label}</label>
    <input class='input-space' id=\"{$campo['campotabela']}\" data-formula='{$campo['formula']}' type=\"text\" name=\"{$campo['campotabela']}\" placeholder=\"{$campo['label']}\" value=\"{$campo['padrao']}\" {$required}>
  </div>";
				break;
			case 'textarea':
				return "<div class=\"field {$blocked}\">
    <label>{$label}</label>
    <textarea class='input-space' data-formula=\"{$campo['formula']}\" id=\"{$campo['campotabela']}\" name='{$campo['campotabela']}' {$required}>{$campo['padrao']}</textarea>
  </div>";
				break;
			case 'numero':
				return "<div class=\"field campo-numero {$blocked}\">
    <label>{$label}</label>
    <input class='input-space' data-formula=\"{$campo['formula']}\" id=\"{$campo['campotabela']}\" type=\"number\" name=\"{$campo['campotabela']}\" value=\"{$campo['padrao']}\" {$required}>
  </div>";
			case 'numeroreal':
				return "<div class=\"field campo-numeroreal {$blocked}\">
    <label>
    	<span style='float: left'>{$label}</span> 
    	{$conversao}
  	</label>
    <input class='input-space' data-formula=\"{$campo['formula']}\" id=\"{$campo['campotabela']}\" type=\"number\" name=\"{$campo['campotabela']}\" placeholder=\"{$campo['sigla']}\" value=\"{$campo['padrao']}\" 
    	step=0.000000000000000001 {$required}>
  </div>";
			case 'data':
				return "<div class=\"field campo-data {$blocked}\">
    <label>{$label}</label>
    <input class='input-space' data-formula=\"{$campo['formula']}\" id=\"{$campo['campotabela']}\" type=\"date\" name=\"{$campo['campotabela']}\" value=\"{$campo['padrao']}\" {$required}>
  </div>";
				break;
			case 'boolean':
				$checked = '';
				if($campo['padrao'] == 1)
					$checked = 'checked';

				return "<div class=\"ui checkbox {$blocked}\">
      <input class='input-space' data-formula=\"{$campo['formula']}\" id=\"{$campo['campotabela']}\" type=\"checkbox\" tabindex=\"0\" class=\"hidden\" name='{$campo['campotabela']}' {$checked}>
      <label>{$label}</label>
    </div><br><br>";
			case 'searchtabaux':
			case 'idtabaux':
				return $this->montaCampoExterno($campo, $required, $label, $vinculos, $restricoes);
				break;
			default:
				return $campo['label'].': Erro de montagem de input';
				break;
		}
	}

	public function montaCampoExterno($campo, $required, $label, $vinculos=[], $restricoes=[])
	{
		$classe = ''; $info = ''; $optionSelf = ''; $valoresVin = []; $restricao = ''; $buttonEdit = ''; $buttonMore = ''; $registros = [];
		$tabExterna = explode('.', $campo['campoaux'])[0];

		$options = "<div class=\"item nenhum\" style='color:red' data-value=''>Nenhum(a)</div>";

		if(in_array($campo['campotabela'], array_column($vinculos, 'campotabela')))
			$valoresVin = $this->Vinculos->getVinculosTabAux($campo['tabela'], $campo['campotabela']);


		foreach ($restricoes as $k => $res) {
			if($res['campotabela'] == $campo['campotabela'])
				$restricao = "data-camporestricao='{$res['camporestricao']}' data-coluna-restricao='{$res['colunarestricao']}'";
		}

		if((empty($this->optionRecords) && !$restricao))
			$registros = $this->Database->getTabauxRecords($campo['campoaux'], $campo['escala']);
		else
			$registros = $this->optionRecords;

		if($tabExterna == 'tipos' || $tabExterna == 'subtipos') {
			$buttonMore = "<div class=\"mini ui icon button green botaonovo-space novo{$tabExterna}\" type='button' data-escala='{$campo['escala']}'  
				data-campotabela='{$campo['campotabela']}' data-tabela='{$tabExterna}' style='vertical-align: bottom; font-size: xx-small;'>
								<i class=\"plus icon\"></i> 
							</div>";
			$buttonEdit = "<div class=\"mini ui icon button orange botaonovo-space novo{$tabExterna} disabled\" type='button' data-escala='{$campo['escala']}' data-edicao='1'
				data-campotabela='{$campo['campotabela']}' data-tabela='{$tabExterna}' style='vertical-align: bottom; font-size: xx-small;'>
								<i class=\"edit icon\"></i> 
							</div>";
		}

		if($campo['tabela'] == $tabExterna)
			$optionSelf = "<div class=\"\" style='float: right; margin-right:' title='O próprio registro a ser inserido'>
								<i class='hand point down icon' style='float: left'></i>
      							<input type=\"checkbox\" tabindex=\"0\" class=\"itself-check\" data-campotabela='{$campo['campotabela']}'}>
      						</div>";

		foreach ($registros as $key => $registro)
		{
			$desc = '';
			$dataVinculo = '';

			foreach ($valoresVin as $i => $vin)
			{
				if($vin['idvalor'] == $registro['id']) {
					$dataVinculo = "data-campoalvo='{$vin['campotabelaalvo']}' data-valorvin='{$vin['valor']}' data-modovin='{$vin['modo']}'";
					break;
				}
			}


			if(isset($registro['descricao']))
			{
				$desc = $registro['descricao'];
				$classe = 'select-tipo type-select';

				$info = " &nbsp; <i class=\"info circle icon\" id='info-{$campo['campotabela']}' title='Sem informação'></i>";
			}

			$options .= "<div class=\"item item-space\" data-value=\"{$registro['id']}\" {$dataVinculo} data-title='{$desc}'>{$registro['nome']}</div>";
		}

		if($tabExterna == 'subtipos')
			$classe = 'select-subtipo type-select';

		if($campo['tipocampo'] == 'searchtabaux')
			$classe .= 'fluid search';

		$this->options = $options;

		return "<div class=\"field\" id='field-select-{$campo['campotabela']}'>
      <label>{$label} {$optionSelf} {$info} {$buttonMore} {$buttonEdit}</label>
      <div class=\"ui {$classe} selection dropdown select-input {$required}\" {$restricao} data-escala=\"{$campo['escala']}\" data-campotabela=\"{$campo['campotabela']}\" data-formula=\"{$campo['formula']}\" id=\"{$campo['campotabela']}\" data-padrao=\"{$campo['padrao']}\">
          <input id='input-{$campo['campotabela']}' class='select-space' type=\"hidden\" data-formula='{$campo['formula']}' name=\"{$campo['campotabela']}\" {$required}>
          <i class=\"dropdown icon\"></i>
          <div class=\"default text\">{$campo['label']}</div>
          <div class=\"menu\">
              {$options}
          </div>
      </div>
  </div>";
	}
}
