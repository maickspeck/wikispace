<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bloqueios extends CI_Model
{
	public function buscaBloqueios($tabela)
	{
		$this->db->select('*');
		$this->db->where('tabela', $tabela);
		return $this->db->get('bloqueios')->result_array();
	}

	public function getBloqueiosModulo($idmodulo, $request)
	{
		$bloqueio = $this->Database->getBy('modulos', $idmodulo, 'id', 'where');

		if(!isset($bloqueio['where']) || !$bloqueio['where'])
			return false;

		$arrBlock = explode('|', $bloqueio['where']);
		$block = !$this->doesMatch($request, $arrBlock[0], $arrBlock[1], $arrBlock[2]);

		setError(!$block,'Campos incompatíveis com o modulo!');

		return $block;
	}

	public function verificaBloqueio($dados, $tabela)
	{
		$bloqueios = $this->buscaBloqueios($tabela);

		$stop = false;

		foreach ($bloqueios as $i => $blok)
		{
			$stopCond = 0; $numCond = 0;

			foreach ($blok as $chave => $cond)
			{
				if(preg_match('/condicao/',$chave))
				{
					if(!$cond)
						continue;

					$numCond++;

					$formula = explode('|', $cond);
					$campotabela = trim($formula[0]);
					$operador = trim($formula[1]);
					$valor = trim($formula[2]);

					if($this->doesMatch($dados, $campotabela, $operador, $valor))
						$stopCond++;
				}
			}

			if($stopCond == $numCond) {
				$stop = true;
				$_SESSION['mensagem']['status'] = 'error';
				$_SESSION['mensagem']['texto'] = $blok['mensagem'];
				break;
			}
		}

		return $stop;
	}

	public function doesMatch($arr,$campo, $op, $val)
	{
		$campotabela = $campo;
		if(isset($arr[$campo]))
			$campo = $arr[$campo];
		else
			$campo = false;

		if(preg_match('/{%}/', $val))
		{
			$campo2 = explode('{%}', $val)[1];
			if(isset($arr[$campo2]))
				$val = $arr[$campo2];
			else
				$val = false;
		}
//		pre($val);
		return doesMatch($op, $campo, $val);
	}
}
